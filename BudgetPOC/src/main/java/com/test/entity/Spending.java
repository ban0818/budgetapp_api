package com.test.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

//Date tempDate = new Date(System.currentTimeMillis());

@Entity
@Table(name = "budget")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Spending {
    @Id
    @Column(name = "id",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    //What month did you buy this
    @Column(name = "month")
    String month;

    //What date did you buy this
    @Column(name = "date")
    String date;

    //How much did you spend
    @Column(name = "amount")
    float amount;

    //How much is left over in the budget
    @Column(name = "total")
    float total;

    //What did you buy
    @Column(name = "item")
    String item;

    //Do you have any notes to yourself
    @Column(name = "notes")
    String notes;
}
