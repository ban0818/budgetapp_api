package com.test.repository;

import com.test.entity.Spending;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpendingRepository extends CrudRepository<Spending,Integer> {
    //Lets you filter the database by the month column
    public Iterable<Spending> findByMonth(String month);
}
