package com.test.controller;

import com.test.entity.Spending;
import com.test.services.SpendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/budget")
public class SpendingController {
    @Autowired
    SpendingService spendingService;

    //Get all entries in the database
    @CrossOrigin
    @GetMapping("")
    public ResponseEntity<Iterable<Spending>> getAllSpending(){
        return new ResponseEntity<>(spendingService.getAllSpending(),HttpStatus.OK);
    }

    //Get all entries within a specific month
    @CrossOrigin
    @GetMapping("/{month}")
    public ResponseEntity<Iterable<Spending>> getMonthSpending(@PathVariable String month){
        return new ResponseEntity<>(spendingService.getMonthSpending(month),HttpStatus.OK);
    }

    //Get the total number of entries in the database
    @CrossOrigin
    @GetMapping("/count")
    public ResponseEntity<Long> getTotalCount(){
        return new ResponseEntity<>(spendingService.getTotalCount(),HttpStatus.OK);
    }

    //Add a new purchase to the database
    @CrossOrigin
    @PostMapping
    public ResponseEntity<Spending> postSpending(@RequestBody Spending spending){
        return new ResponseEntity<>(spendingService.postSpending(spending),HttpStatus.OK);
    }

    //Clear out the database
    @CrossOrigin
    @DeleteMapping
    public ResponseEntity<HttpStatus> deleteAllSpending(){
        return new ResponseEntity<>(spendingService.deleteAllSpending(),HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable Integer id){
        return new ResponseEntity<>(spendingService.deleteById(id),HttpStatus.OK);
    }
}
