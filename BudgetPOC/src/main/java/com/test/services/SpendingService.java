package com.test.services;

import com.test.entity.Spending;
import org.springframework.http.HttpStatus;

public interface SpendingService {
    public Iterable<Spending> getAllSpending();
    public Iterable<Spending> getMonthSpending(String month);
    public Spending postSpending(Spending spending);
    public HttpStatus deleteAllSpending();
    public HttpStatus deleteById(Integer id);
    public Long getTotalCount();
}
