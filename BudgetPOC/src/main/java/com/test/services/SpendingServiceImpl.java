package com.test.services;

import com.test.entity.Spending;
import com.test.repository.SpendingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
@Slf4j
public class SpendingServiceImpl implements SpendingService {
    @Autowired
    SpendingRepository spendingRepository;

    //Return all entries in database
    @Override
    public Iterable<Spending> getAllSpending(){
        log.info("Getting all Spending");
        return spendingRepository.findAll();
    }

    //Add a new item to the database
    @Override
    public Spending postSpending(Spending spending){
        log.info("Adding an entry to the database");
        return spendingRepository.save(spending);
    }

    //Filter by a specific month
    @Override
    public Iterable<Spending> getMonthSpending(String month){
        log.info("Getting all Spending in a specific month");
        return spendingRepository.findByMonth(month);
    }

    //Delete all information and return a http to let the user know it was finished
    @Override
    public HttpStatus deleteAllSpending(){
        log.info("Deleting all data...");
        spendingRepository.deleteAll();
        return HttpStatus.ACCEPTED;
    }

    //Delete specific entry
    @Override
    public HttpStatus deleteById(Integer id){
        log.info("Deleting entry...");
        spendingRepository.deleteById(id);
        return HttpStatus.ACCEPTED;
    }

    //Get the amount of entries in the database
    @Override
    public Long getTotalCount(){
        log.info("Getting total number of entries");
        return spendingRepository.count();
    }
}
